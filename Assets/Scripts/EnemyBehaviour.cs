﻿using UnityEngine;
using System;

public abstract class EnemyBehaviour : MonoBehaviour
{
    // Use this for initialization
    void Awake()
    {
        OnAwake();
    }

    protected abstract void OnAwake();

    public abstract void Behave();
}
