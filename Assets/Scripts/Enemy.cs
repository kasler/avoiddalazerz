﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
	Vector3 initialPos;

	// Use this for initialization
	void Awake()
	{
		Init();
	}

	protected virtual void Init()
	{
		initialPos = transform.position;
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		GameManager.Instance.Hit();
	}

	public virtual void Spawn()
	{
		transform.position = initialPos;
	}

	public void SpawnAt(Vector3 where)
	{
		transform.position = where;

		Init();
	}
}
