﻿using UnityEngine;
using System.Collections;

public class Spin : Scroll
{
    public bool clockWiseDirection;
    public bool randomDirection;

    public override void Behave()
    {
        body.angularDrag = 0;

        if (randomDirection)
            if (Random.Range(0, 2) == 0)
                clockWiseDirection = true;
            else
                clockWiseDirection = false;
 
        GenerateRandomSpeed();

        if (clockWiseDirection)
            body.AddTorque(speed);
        else
            body.AddTorque(-speed);
    }
}