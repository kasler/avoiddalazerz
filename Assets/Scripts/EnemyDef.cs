﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    class EnemyDef
    {
        private string name;
        private int startingLevel;
        private int topLevel;
        private string[] getsAlongWith;

        public EnemyDef(string name, int startingLevel, int topLevel, string[] getsAlongWith)
        {
            this.name = name;
            this.startingLevel = startingLevel;
            this.topLevel = topLevel;
            this.getsAlongWith = getsAlongWith;
        }
    }
}
