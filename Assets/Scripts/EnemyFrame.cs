﻿using UnityEngine;
using System.Collections;

public class EnemyFrame : Enemy
{
	protected Enemy mainEnemy;
    protected Spawner spawner;
    public GameObject followingEnemy;
    Vector2 v;
    float xDistance;

	// Use this for initialization
	void Start ()
	{
        spawner = GameObject.FindObjectOfType<Spawner>();
        mainEnemy = transform.parent.GetComponent<Enemy>();

        if (followingEnemy != null)
            xDistance = transform.position.x - followingEnemy.transform.position.x;
	}

    void Awake()
    {
        Init();
    }

    void Update()
    {
        if (followingEnemy)
        {
            v.x = followingEnemy.transform.position.x + xDistance;
            v.y = 0;
            transform.position = v;
        }
    }

	void OnTriggerEnter2D(Collider2D collider)
	{
        // If it's half screen, get another enemy.
        // Else we just quit the screen
        if (collider.tag == "Half")
            spawner.DePool();
        else
		    spawner.Pool(mainEnemy);
	}
}
