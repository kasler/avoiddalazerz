﻿using UnityEngine;
using System.Collections;

public class MultiScroll : Scroll
{
    public override void Behave()
    {
        GenerateRandomSpeed();
        
        foreach (var curBody in GetComponentsInChildren<Rigidbody2D>())
        {
            curBody.angularDrag = 0;

            curBody.AddForce(-Vector2.right * speed);
        }
    }
}
