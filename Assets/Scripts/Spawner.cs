﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets;
using System.Linq;

public class Spawner : MonoBehaviour
{
    private const int INFINITY = -1;
    private const float LEVRL_UP_TIME = 10;
    private const int SPEED_UP_RATE = 2;
    private const float SPEED_INCREASE_MULTIPLYER = 1.2f;

    public static int level;
    public static float speedMultiplyer;

    List<Enemy> enemiesList;
    Dictionary<string, LinkedList<Enemy>> enemyPoolsDict;
    Dictionary<string, EnemyDef> enemiesDefinitionDict;
    private float levelStartTime;
    private Enemy newestEnemy;

	// Use this for initialization
	void Start ()
    {
        enemyPoolsDict = new Dictionary<string,LinkedList<Enemy>>();
        enemiesDefinitionDict = new Dictionary<string, EnemyDef>();
	    enemiesList = new List<Enemy>();

        levelStartTime = Time.time;
        level = 1;
        speedMultiplyer = 1;

        RegisterAllEnemies();

        // First enemy
        DePool();
	}

    private void RegisterAllEnemies()
    {
        // Add Lasers
        RegisterEnemy("Laser", 1, INFINITY, 5, new string[] { "Star", "MegaStar" });

        // Add Stars
        RegisterEnemy("Star", 1, INFINITY, 5, new string[] { "Star", "MegaStar" });

        // Add MegaStars
        RegisterEnemy("StarHigh", 1, INFINITY, 5, new string[] { "Star", "MegaStar" });

        // Add BigStars
        RegisterEnemy("BigStar", 1, INFINITY, 5, new string[] { "Star", "MegaStar" });

        // Add MegaStars
        RegisterEnemy("MegaStar", 1, INFINITY, 5, new string[] { "Star", "MegaStar" });
    }

    private void RegisterEnemy(string name, int startingLevel, int topLevel, int maxSimultanousInstances, string[] getsAlongWith)
    {
        // Create the new definition for this enemy
        enemiesDefinitionDict.Add(name, new EnemyDef(name, startingLevel, topLevel, getsAlongWith));

        LinkedList<Enemy> enemyPool = new LinkedList<Enemy>();

        // Load him
        GameObject enemyInstance =  ((GameObject)Resources.Load("Enemies/" + name));

        // And instantiate a pool of him
        for (int i = 1; i < maxSimultanousInstances; i++)
        {
            Enemy clone = ((GameObject)Instantiate(enemyInstance, enemyInstance.transform.position, Quaternion.identity)).GetComponent<Enemy>();
            clone.name = name;
            clone.gameObject.SetActive(false);
            enemyPool.AddFirst(clone);
            enemiesList.Add(clone);
        }

        // Finally make it's pool an entry in the pools dict
        enemyPoolsDict.Add(name, enemyPool);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(1))
            DePool();

        if (Time.time - levelStartTime > LEVRL_UP_TIME)
            LevelUp();
	}

    private void LevelUp()
    {
        ++level;
        
        levelStartTime = Time.time;

        // speed up every SPEED_UP_RATE levels
        if (level % SPEED_UP_RATE == 0)
            speedMultiplyer *= SPEED_INCREASE_MULTIPLYER;
    }

	public void Respawn(Enemy enemy)
	{
		foreach (var childEnemy in enemy.GetComponentsInChildren<Enemy>())
			childEnemy.Spawn();

        foreach (var childEnemy in enemy.GetComponentsInChildren<EnemyBehaviour>())
            childEnemy.Behave();
	}

    public void Pool(Enemy enemy)
    {
        enemy.gameObject.SetActive(false);

        // Return him to his pool
        enemyPoolsDict[enemy.gameObject.name].AddFirst(enemy);
    }

    public void DePool()
    {
        // Get a random enemy key
        List<LinkedList<Enemy>> values = Enumerable.ToList(enemyPoolsDict.Values);
        int rand = Random.Range(0, values.Count);
        newestEnemy = values[rand].First.Value;

        // Take it outta' the list
        values[rand].RemoveFirst();

        newestEnemy.gameObject.SetActive(true);
        Respawn(newestEnemy);
    }

    public float GetSpeedMultiplyer()
    {
        return speedMultiplyer;
    }
}
