﻿using UnityEngine;
using System.Collections;

public class LaserTarget : EnemyNoContact
{
    const float HEAT_UP_TIME = 1.7f;

    Player player;
    Laser laser;
    private bool lockedOn;
    private bool initializing;
    private Vector3 v;
    private float lastRespawnTime;
    private SpriteRenderer sprite;

	// Use this for initialization
	void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();

        Init();
    }

    protected override void Init()
    {
 	    base.Init();

        OnSpawn();
	}

    public override void Spawn()
    {
        base.Spawn();

        OnSpawn();
    }

    public void OnSpawn()
    {
        sprite.enabled = false;

        lastRespawnTime = Time.time;

        GameManager.Instance.Refresh();

        v = transform.position;

        player = GameManager.Instance.GetPlayer();
        laser = transform.parent.GetComponentInChildren<Laser>();

        initializing = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (lockedOn)
            return;

        if (initializing)
        {
            v.x = laser.transform.position.x;
            print(v.x);
            //transform.position = v;

            if (Time.time - lastRespawnTime > HEAT_UP_TIME)
            {
                initializing = false;
                sprite.enabled = true;
            }
            else
                return;
        }
       
        laser.ShowLockBeam(false);

        var origRotation = transform.rotation;

        //rotate to look at the player
        transform.rotation = Quaternion.Slerp(transform.rotation,
                                                Quaternion.LookRotation(player.transform.position - transform.position), 0.9f);

        //move towards the player

        var mulriplyer = 3;

        if (IsLine(transform.position, player.transform.position, laser.transform.position))
        {
            mulriplyer = 5;
            laser.ShowLockBeam(true);
        }

        transform.position += transform.forward * mulriplyer * Mathf.Max(2, Vector2.Distance(transform.position, player.transform.position)) * Time.deltaTime;

        transform.rotation = origRotation;
	}

    bool IsLine(Vector3 a, Vector3 b, Vector3 c)
    {
        return (Mathf.Abs((a.y - b.y) * (a.x - c.x) - (a.y - c.y) * (a.x - b.x)) < 2);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (lockedOn)
            return;

        lockedOn = true;
        
        StartCoroutine(EngageShoot());
    }

    private IEnumerator EngageShoot()
    {
        yield return new WaitForSeconds(0.1f);

        laser.ShowLockBeam(false);
        laser.Shoot();

        StartCoroutine(RelockOn());
    }

    private IEnumerator RelockOn()
    {
        yield return new WaitForSeconds(0.5f);

        lockedOn = false;
    }
}
