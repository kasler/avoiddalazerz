﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour
{
	float lastRotationTime;
	float lastRegistered;
	GameObject beam;
    LaserTarget laserTarget;
    private GameObject lockBeam;

	// Use this for initialization
	void Start ()
	{
        laserTarget = transform.parent.GetComponentInChildren<LaserTarget>();

        foreach (var c in GetComponentsInChildren<Transform>())
        {
            if (c.parent == transform && c.gameObject.name == "Beam")
            {
                beam = c.gameObject;
                beam.SetActive(false);
            }

            if (c.parent == transform && c.gameObject.name == "LockBeam")
            {
                lockBeam = c.gameObject;
                lockBeam.SetActive(false);
            }
        }
	}
	
	// Update is called once per frame
	void Update ()
	{
        float AngleDeg = (180 / Mathf.PI) * Mathf.Atan2(laserTarget.transform.position.y - transform.position.y, laserTarget.transform.position.x - transform.position.x) + 180;
        var goalRotation = Quaternion.Euler(0, 0, AngleDeg);
        this.transform.rotation = goalRotation;
	}

	public void Shoot()
	{
		beam.SetActive(true);
        StartCoroutine(BeamAnimation());
	}

    private IEnumerator BeamAnimation()
    {
        yield return new WaitForSeconds(0.1f);

        beam.SetActive(false);
    }

    public void ShowLockBeam(bool show)
    {
        if (lockBeam.activeSelf != show)
            lockBeam.SetActive(show);
    }
}
