﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoSingleton<GameManager>
{
    Player player;

	// Use this for initialization
	void Awake ()
    {
        Application.targetFrameRate = 60;
        //QualitySettings.vSyncCount = 0;
        player = GameObject.FindObjectOfType<Player>();

        StartGame();
	}

    private void StartGame()
    {
    }
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Hit()
	{
        StopGame();
	}

    private void StopGame()
    {
        Application.LoadLevel("PlayScene");
    }

    public Player GetPlayer()
    {
        return player;
    }

    public void Refresh()
    {
        player = GameObject.FindObjectOfType<Player>();
    }
}
