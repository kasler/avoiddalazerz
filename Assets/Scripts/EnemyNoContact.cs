﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class EnemyNoContact : Enemy
{
    void Awake ()
	{
		base.Init();
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
        // DOES NOT HIT THE PLAYER
	}

}