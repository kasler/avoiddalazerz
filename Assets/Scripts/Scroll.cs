﻿using System;
using UnityEngine;

public class Scroll : EnemyBehaviour
{
	protected Rigidbody2D body;
	public int minSpeed;
    public int maxSpeed;
    protected float speed;

    protected override void OnAwake()
    {
        if (minSpeed > maxSpeed)
            throw new ArgumentException("minSpeed can't be bigger than maxSpeed");

        body = GetComponent<Rigidbody2D>();

        Behave();
    }

    public override void Behave()
    {
        body.angularDrag = 0;

        GenerateRandomSpeed();

        body.AddForce(-Vector2.right * speed);
    }

    protected void GenerateRandomSpeed()
    {
        // Getting a random speed in the given range times game speed multiplyer
        speed = UnityEngine.Random.Range(minSpeed, maxSpeed + 1) * Spawner.speedMultiplyer;
    }

    public float GetSpeed()
    {
        return speed;
    }
}
